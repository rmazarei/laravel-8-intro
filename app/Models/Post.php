<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    public $appends = ['jalaliDate', 'tea'];
//    public $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getJalaliDateAttribute()
    {
        return verta($this->created_at)->format('%B %d، %Y');
    }

    public function getTeaAttribute()
    {
        return 'Sweet Tea';
    }
}
