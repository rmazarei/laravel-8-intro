<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;  // IT'S NOT A GOOD IDEA
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cover' => 'nullable|mimes:jpg,png,gif,jpeg',
            'title' => 'required|min:8|max:10',
            'body'  => 'required'
        ];
    }
}
