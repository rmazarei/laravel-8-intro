@extends('layouts.main')

@section('title', $post->title)

@section('content')
    <h1>{{ $post->title }}</h1>
    <p>{{ $post->body }}</p>
    <em> create at:
        {{ $post->jalali_date }}
        {{ $post->tea }}
    </em>
@endsection
