@extends('layouts.main')

@section('title', 'Create New Post')

@section('content')
    <h1>Create new post</h1>
    <form action="/posts/{{ $post->id }}" method="post">
        @method('PUT')
        @csrf
        <input type="text" name="title" value="{{ $post->title }}"><br>
        <textarea name="body" id="" cols="30" rows="10">{{ $post->body }}</textarea><br>
        <input type="submit" value="Update">
    </form>
@endsection
