@extends('layouts.main')

@section('title', 'Create New Post')

@section('content')
    <h1>Create new post</h1>

    @if($errors->any())
        <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    @endif
    <div id="editor"></div>
    <form action="/posts" method="post" enctype="multipart/form-data">
        @csrf
        <input type="file" name="cover"><br>
        <input type="text" name="title" value="{{ old('title') }}"><br>
        <textarea name="body" id="" cols="30" rows="10">{{ old('body') }}</textarea><br>
        <input type="submit" value="Create">
    </form>
@endsection
