@extends('layouts.main')

@section('content')
    <div class="container">
    <h1>Posts List</h1>

        @auth
        <a href="/posts/create">add new post</a>
        @endauth
        @guest
            Please login/register to create new post
        @endguest
{{--
    <p style="color: red;">
        @auth
            @if(Auth::id() === 1)
        You are our 1st user
            @else
                You are not our 1st user
            @endif
        @endauth
    </p>
    --}}

    @foreach($posts as $post)
        <img src="/storage/images/{{ $post->cover }}" alt="" width="100">
        <h3><a href="/posts/{{ $post->id }}">{{ $post->title }}</a> ({{ $post->views }})</h3>
        created by: {{ $post->user ?  $post->user->name : ''}} <br>

<!--
        @if($post->user)
            {{ $post->user->name }}
        @else
            no name
        @endif
        -->

        <a href="/posts/{{ $post->id }}/edit" class="text-success">edit post</a>
        <form action="/posts/{{ $post->id }}" method="post">
            @method('delete')
            @csrf
            <input type="submit" value="Delete" class="text-danger">
        </form>

        <hr>
    @endforeach
    </div>

    <post-list></post-list>
@endsection
